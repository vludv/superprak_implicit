#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cstdio>
#include <cassert>
#include <omp.h>
#include <mpi.h>
#include "task_def.hpp"
using namespace std;

#define pprintf(...) \
{\
    int idx_proc;                               \
    MPI_Comm_rank(MPI_COMM_WORLD, &idx_proc);   \
    printf("%03d:", idx_proc);                  \
    printf(__VA_ARGS__);                        \
    fflush(stdout);                             \
}

class Mat
{
public:
    int h, w;
    double *data_raw, *data;

    Mat()
    {
        h = w = 0;
        data_raw = data = NULL;
    }

    Mat(int h_, int w_)
    {
        h = h_;
        w = w_;

        data_raw = new double[h*w];
        data = data_raw;
    }

    void Create(int h_, int w_)
    {
        if (data_raw != NULL)
            delete[] data_raw;

        h = h_;
        w = w_;

        data_raw = new double[h*w];
        data = data_raw;
    }

    void SetTo(double value = 0)
    {
        for (int i = 0; i < w*h; i++)
            data_raw[i] = value;
    }

    ~Mat()
    {
        delete[] data_raw;
        h = w = 0;
        data_raw = data = NULL;
    }

    inline double &operator()(int i, int j)
    {
        return data[i*w + j];
    }

    inline double *ptr(int i = 0, int j = 0)
    {
        return data_raw + i*w + j;
    }
    
    inline double &at(int i, int j)
    {
        return data_raw[i*w + j];
    }

    void SetAnchor(int i0=0, int j0=0)
    {
        data = data_raw - i0*w - j0;
    }

    void DumpCSV(const string &filename)
    {
        FILE *f = fopen(filename.c_str(), "w");

        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                fprintf(f, "%.3lf", at(i, j));
                if (j < w - 1)
                    fprintf(f, ",");
            }

            if (i < h - 1)
                fprintf(f, "\n");
        }

        fclose(f);
    }
};

double getSolutionError(Mat &u, double t)
{
    double err = 0;
    double nrm = u.h - 1;
    assert(u.h == u.w);

    for (int i = 0; i < u.h; i++)
        for (int j = 0; j < u.w; j++)
        {
            double ref = TaskDef::u(i / nrm, j / nrm, t);
            double est = u.at(i, j);
            err = std::max(err, abs(ref - est));
        }

    return err;
}

class SolverMPI : public ISolver
{
public:
    int iter;
    double t0;
    double err_total;
    int print_each;
    int num_omp_procs;

protected:
    Mat u_curr, u_half, u_next;
    Mat u_glob;

    int idx_proc, num_procs;
    vector<int> proc_starts, proc_sizes;
    
    int range_start, range_end;
    int range_start_ext, range_end_ext;
    int range_size, range_size_ext;
    int over_first, over_last; //overlapping of first and last row

    double h2tau, h2ktau, h2k;

    MPI_Status status;
    MPI_Request request;

    vector<double> sb_c_, sb_fcoef_, sb_xcoef_bwd, sb_fcoef_bwd; //simple pass auxilary
    Mat sb_fbuf;

    //parallel blocks
    vector<double> pb_a, pb_g, pb_f;
    vector<double> pb_dcoef_fwd, pb_dcoef_bwd;
    Mat pb_d;

    //linear system responsibilities
    vector<int> procs_ls_start, procs_ls_size;
    int ls_start, ls_size, ls_i0;

    //merge
    vector<double> pm_a, pm_b, pm_c, pm_c_, pm_coef;
    Mat pm_d, pm_x;

    bool pb_use_all2all;
    vector<int> pm_dsnd_counts, pm_dsnd_displ, pm_drcv_counts, pm_drcv_displs;
    vector<int> pm_xsnd_counts, pm_xsnd_displs, pm_rcv_back_counts, pm_rcv_back_displs;
    vector<double> x_fst_lst_buf;
    int pm_col_j0, pm_col_sz;

    inline double b0(int i)
    {
        assert(i > 0 && i < n_ext);
        return (i != n_ext - 1) ? 1 : 0;
    }
    inline double a0(int i)
    {
        assert(i >= 0 && i < n_ext);
        return (i == 0 || i == n_ext - 1) ? 1 : (-2 - h2ktau);
    }
    inline double c0(int i)
    {
        assert(i >= 0 && i < n_ext - 1);
        return (i != 0) ? 1 : 0;
    }

    static void Compute2Diag(const vector<double> &a, const vector<double> &c, const vector<double> &b, vector<double> &c_, vector<double> &cf)
    {
        c_[0] = c[0];
        cf[0] = 0;
        for (int i = 1; i < (int)a.size(); i++)
        {
            cf[i] = -a[i] / c_[i - 1];
            c_[i] = c[i] - a[i] * b[i - 1] / c_[i - 1];
        }
    }

    void FillLeftCoefsFwd(int i0, int sz)
    {
        pb_a[i0] = a0(i0);
        pb_dcoef_fwd[i0] = 0; //don't use

        if (i0 != 0)
            pb_f[i0] = b0(i0);

        for (int i = i0+1; i < i0 + sz; i++)
        {
            pb_dcoef_fwd[i] = - b0(i)/pb_a[i-1];
            pb_a[i] = a0(i) - c0(i-1)* b0(i)/pb_a[i-1];
            pb_f[i] = - pb_f[i-1] * b0(i)/pb_a[i-1];
        }

        if (i0 == 0) //first block don't use f
            for (int i = i0; i < i0 + sz; i++)
                pb_f[i] = 0;
    }

    void FillLeftCoefsBwd(int i0, int sz)
    {
        //pb_g[i0 + sz - 1] = pb_a[sz - 1];
        pb_g[i0 + sz - 2] = c0(i0 + sz - 2);
        pb_dcoef_bwd[i0 + sz - 1] = 0;
        pb_dcoef_bwd[i0 + sz - 2] = 0;

        for (int i = i0 + sz - 3; i >= i0; i--)
        {
            pb_dcoef_bwd[i] = -c0(i)/pb_a[i+1];
            pb_g[i] = -pb_g[i+1] * c0(i)/pb_a[i+1];
            pb_f[i] = pb_f[i] - c0(i)*pb_f[i+1]/pb_a[i+1];
        }

        if (i0 > 0)
        {
            assert(pb_a[i0] == a0(i0));

            pb_dcoef_bwd[i0-1] = -c0(i0-1) / pb_a[i0];
            pb_g[i0-1] = -pb_g[i0] * c0(i0-1) / pb_a[i0];
            pb_a[i0-1] = pb_a[i0-1] - pb_f[i0] * c0(i0-1) / pb_a[i0];
        }
    }

    void FillLeftCoefs(int i0, int sz)
    {
        FillLeftCoefsFwd(i0, sz);
        FillLeftCoefsBwd(i0, sz);
    }

    void InitLeftPartData()
    {
        pb_a.resize(n_ext, NAN);
        pb_g.resize(n_ext, NAN);
        pb_f.resize(n_ext, NAN);
        pb_dcoef_fwd.resize(n_ext, NAN);
        pb_dcoef_bwd.resize(n_ext, NAN);

        procs_ls_start.resize(num_procs);
        procs_ls_size.resize(num_procs);
        
        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            bool is_first = (i_proc == 0);
            bool is_last = (i_proc == num_procs - 1);

            int i0 = procs_ls_start[i_proc] = proc_starts[i_proc] - ((is_first) ? 1 : 0);
            int sz = procs_ls_size[i_proc] = proc_sizes[i_proc] + ((is_first || is_last) ? 1 : 0);

            FillLeftCoefs(i0, sz);
        }

        ls_start = procs_ls_start[idx_proc];
        ls_size = procs_ls_size[idx_proc];
        ls_i0 = (idx_proc == 0) ? 0 : 1;
    }

    void InitMergeData()
    {
        pm_a.resize(num_procs);
        pm_c.resize(num_procs);
        pm_c_.resize(num_procs);
        pm_b.resize(num_procs);
        pm_coef.resize(num_procs);

        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            int idx = procs_ls_start[i_proc] + procs_ls_size[i_proc] - 1;
            pm_a[i_proc] = pb_f[idx];
            pm_c[i_proc] = pb_a[idx];
            pm_b[i_proc] = pb_g[idx];
        }
        pm_a[0] = pm_b[num_procs - 1] = NAN;

        Compute2Diag(pm_a, pm_c, pm_b, pm_c_, pm_coef);


        if (!pb_use_all2all)
        {
            pm_d.Create(num_procs, n_ext);
            pm_x.Create(num_procs, n_ext);
            return;
        }
        
        //Setup AllToAll buffers
        pm_dsnd_counts.resize(num_procs);
        pm_dsnd_displ.resize(num_procs);
        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            int start, end, sz;
            sz = GetRange(i_proc, num_procs, n_ext - 2, start, end);
            start += 1;
            end += 1;

            pm_dsnd_counts[i_proc] = sz;
            pm_dsnd_displ[i_proc] = start;
        }

        pm_col_j0 = pm_dsnd_displ[idx_proc];
        pm_col_sz = pm_dsnd_counts[idx_proc];

        pm_d.Create(num_procs, pm_col_sz);
        pm_x.Create(num_procs, pm_col_sz);

        pm_drcv_counts.resize(num_procs);
        pm_drcv_displs.resize(num_procs);
        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            pm_drcv_counts[i_proc] = pm_col_sz;
            pm_drcv_displs[i_proc] = pm_col_sz * i_proc;
        }

        x_fst_lst_buf.resize(2*n_ext, NAN);
        pm_xsnd_counts.resize(num_procs);
        pm_xsnd_displs.resize(num_procs);
        pm_rcv_back_counts.resize(num_procs);
        pm_rcv_back_displs.resize(num_procs);

        int sum = 0;
        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            pm_xsnd_counts[i_proc] = (i_proc != 0) ? 2*pm_col_sz : pm_col_sz;
            pm_xsnd_displs[i_proc] = (i_proc != 0) ? (i_proc-1) * pm_col_sz : i_proc * pm_col_sz;
            pm_rcv_back_counts[i_proc] = (idx_proc != 0) ? 2*pm_dsnd_counts[i_proc] : pm_dsnd_counts[i_proc];
            pm_rcv_back_displs[i_proc] = sum;
            sum += pm_rcv_back_counts[i_proc];
        }
    }

    void InitRanges()
    {
        int start, end;

        GetRange(idx_proc, num_procs, n, start, end);
        
        range_start = start + 1;
        range_end = end + 1;
        range_size = range_start - range_end;

        range_start_ext = range_start - 1;
        range_end_ext = range_end + 1;
        range_size_ext = range_end_ext - range_start_ext;

        over_first = (idx_proc == 0) ? 0 : 1;
        over_last = (idx_proc == n_ext - 1) ? 0 : 1;

        proc_starts.resize(num_procs);
        proc_sizes.resize(num_procs);
        for (int i_proc = 0; i_proc < num_procs; i_proc++)
        {
            GetRange(i_proc, num_procs, n, start, end);
            proc_starts[i_proc] = start + 1;
            proc_sizes[i_proc] = end - start;

            assert(!(i_proc != 0) || (proc_starts[i_proc] == proc_starts[i_proc-1] + proc_sizes[i_proc-1]));
        }
    }

    inline bool is_master()
    {
        return (idx_proc == 0);
    }

    static inline int align(int size, int bsize)
    {
        return ((size + bsize-1)/bsize)*bsize;
    }

    void InitSimpleCoefs()
    {
        sb_c_.resize(n_ext);
        sb_fcoef_.resize(n_ext);
        
        sb_c_[0] = C(0);
        sb_fcoef_[0] = NAN;
        for (int j = 1; j < n_ext; j++)
        {
            sb_c_[j] = C(j) - A(j) * B(j - 1) / sb_c_[j - 1];
            sb_fcoef_[j] = -A(j) / sb_c_[j - 1];
        }

        sb_fbuf.Create(num_omp_procs, align(n_ext, 8));
    }
    //definition of 3-diagonal matrix
    double A(int i)
    {
        return (0 < i && i < n_ext - 1) ? 1 : 0;
    }
    double C(int i)
    {
        return (0 < i && i < n_ext - 1) ? (-2 - h2ktau) : 1;
    }
    double B(int i)
    {
        return (0 < i && i < n_ext - 1) ? 1 : 0;
    }

public:

    SolverMPI(int n, int max_omp_threads = -1) :
        ISolver(n)
    {
        MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
        MPI_Comm_rank(MPI_COMM_WORLD, &idx_proc);

        if (max_omp_threads > 0)
            omp_set_num_threads(max_omp_threads);
        num_omp_procs = omp_get_max_threads();

        h2tau = 2 * h*h / tau;
        h2ktau = 2 * h*h / (tau * k);
        h2k = h*h / k;

        print_each = 100;
        pb_use_all2all = true;

        InitRanges();
        InitLeftPartData();
        InitMergeData();

        u_curr.Create(range_size_ext, n_ext);
        u_next.Create(range_size_ext, n_ext);
        pb_d.Create(range_size_ext, align(n_ext, 8));
        pb_d.SetTo(NAN);
        pm_d.SetTo(NAN);
        pm_x.SetTo(NAN);

        if (is_master())
            u_glob.Create(n_ext, n_ext);

        InitSimpleCoefs();
    }

    static int GetRange(int idx_proc, int num_procs, int n, int &start, int &end) // [start; end)
    {
        int unallocated = n % num_procs;
        int base_chunk = n / num_procs;
        int chunk = base_chunk + ((idx_proc < unallocated) ? 1 : 0);

        start = idx_proc * base_chunk + min(idx_proc, unallocated);
        end = min(start + chunk, n);
        
        return end - start;
    }

    static inline int GetRange(int idx_proc, int num_procs, int r_start, int r_end, int &start, int &end) // [start; end)
    {
        assert(r_start < r_end);
        GetRange(idx_proc, num_procs, r_end - r_start, start, end);
        start += r_start;
        end += r_start;
        return end - start;
    }

    double getApproxError(Mat &u0, Mat &u1, double tf, bool half = true)
    {
        double err = 0;
        for (int i = 1; i < range_size_ext - 1; i++)
        {
            int i_glob = i + range_start_ext;

            for (int j = 1; j < n_ext - 1; j++)
            {
                double l = (u1.at(i, j) - u0.at(i, j)) / (tau / 2);
                double r = TaskDef::f(i_glob*h, j*h, tf);
                if (half)
                    r += TaskDef::k()*(u1(i, j - 1) - 2 * u1(i, j) + u1(i, j + 1) + u0(i - 1, j) - 2 * u0(i, j) + u0(i + 1, j)) / (h*h);
                else
                    r += TaskDef::k()*(u0(i, j - 1) - 2 * u0(i, j) + u0(i, j + 1) + u1(i - 1, j) - 2 * u1(i, j) + u1(i + 1, j)) / (h*h);

                err = max(err, abs(l - r));
            }
        }

        return err;
    }

    double getError(Mat &u, double tu, bool with_borders = true, vector<double> *per_row_error = NULL)
    {
        double err = 0;
        int brd = with_borders ? 0 : 1;
        int ofs = (u.h == n_ext) ? 0 : range_start_ext;

        if (per_row_error)
            per_row_error->assign(u.h, NAN);

        for (int i = brd; i < u.h - brd; i++)
        {
            double row_err = 0;
            for (int j = brd; j < u.h - brd; j++)
            {
                double u_exact = TaskDef::u((ofs + i)*h, j*h, tu);
                row_err = max(row_err, abs(u_exact - u.at(i, j)));
            }
            err = max(err, row_err);

            if (per_row_error)
                per_row_error->at(i) = row_err;
        }

        return err;
    }

    void SetU(Mat &u, double t, int ofs_i = 0, int ofs_j = 0)
    {
        #pragma omp parallel for
        for (int i = 0; i < u.h; i++)
            for (int j = 0; j < u.w; j++)
                u.at(i, j) = TaskDef::u((i + ofs_i)*h, (j + ofs_j)*h, t);
    }

    void MakeHorPass(Mat &u, Mat &u_nxt, double t0)
    {
        double tu = t0;
        double tf = t0 + tau/2;

        //set bounds
        if (range_start_ext == 0)
        {
            #pragma omp parallel for schedule(static)
            for (int j = 0; j < n_ext; j++)
                u_nxt.at(0, j) = TaskDef::u(0, j*h, tu);
        }
        if (range_end_ext == n_ext)
        {
            #pragma omp parallel for schedule(static)
            for (int j = 0; j < n_ext; j++)
                u_nxt.at(range_size_ext - 1, j) = TaskDef::u(1, j*h, tu);
        }

        #pragma omp parallel for
        for (int i = 1; i < range_size_ext - 1; i++)
        {
            int i_thread = omp_get_thread_num();
            int i_glob = range_start_ext + i;
            assert(i_thread < num_omp_procs);

            double *x = u_nxt.ptr(i);
            double *f_ = sb_fbuf.ptr(i_thread);
            double x0 = TaskDef::u(i_glob*h, 0, tu);
            double x1 = TaskDef::u(i_glob*h, 1, tu);

            f_[0] = x0;
            for (int j = 1; j < n_ext - 1; j++)
            {
                double f = -h2k*TaskDef::f(i_glob*h, j*h, tf) - u.at(i - 1, j) + (2.0 - h2ktau)*u.at(i, j) - u.at(i + 1, j);
                f_[j] = f + f_[j - 1] * sb_fcoef_[j];
            }

            x[n_ext - 1] = x1;
            for (int j = n_ext - 2; j >= 0; j--)
                x[j] = (f_[j] - B(j) * x[j + 1]) / sb_c_[j];
            assert(abs(x0 - x[0]) < EPS);
        }

        SyncOverlappedRows(u_nxt, TAG_SYNC_OVERLAPPED_ROWS_HOR);
    }

    enum Tags
    {
        TAG_SYNC_OVERLAPPED_ROWS_HOR = 0,
        TAG_D_NBR_UPDATE,
        TAG_D_TO_MERGER,
        TAG_X_FROM_MERGER1,
        TAG_X_FROM_MERGER2,
        TAG_SYNC_OVERLAPPED_ROWS_VER,
        TAG_MERGE_TO_MASTER,
        TAG_NONE = 10
    };

    int get_tag(int type, int id)
    {
        return id*TAG_NONE + type;
    }

    void SyncOverlappedRows(Mat &u, int tag)
    {
        if (idx_proc > 0)
            MPI_Isend(u.ptr(1), n_ext, MPI_DOUBLE, idx_proc-1, get_tag(tag, iter), MPI_COMM_WORLD, &request);

        if (idx_proc < num_procs-1)
            MPI_Isend(u.ptr(range_size_ext-2), n_ext, MPI_DOUBLE, idx_proc+1, get_tag(tag, iter), MPI_COMM_WORLD, &request);

        if (idx_proc > 0)
            MPI_Recv(u.ptr(0), n_ext, MPI_DOUBLE, idx_proc-1, get_tag(tag, iter), MPI_COMM_WORLD, &status);

        if (idx_proc < num_procs - 1)
            MPI_Recv(u.ptr(range_size_ext-1), n_ext, MPI_DOUBLE, idx_proc+1, get_tag(tag, iter), MPI_COMM_WORLD, &status);
    }

    Mat &MergeToMaster(Mat &u, Mat &u_glob)
    {
        if (idx_proc == 0)
        {
            assert(u_glob.h == n_ext && u_glob.w == n_ext);

            vector<int> glob_merge_displs, merge_glob_counts;
            glob_merge_displs.resize(num_procs);
            merge_glob_counts.resize(num_procs);
            int iofs = 0;
            for (int i = 0; i < num_procs; i++)
            {
                merge_glob_counts[i] = procs_ls_size[i] * n_ext;
                glob_merge_displs[i] = iofs;
                iofs += merge_glob_counts[i];
            }
            
            MPI_Gatherv(u.ptr(over_first), ls_size*n_ext, MPI_DOUBLE, u_glob.ptr(), &merge_glob_counts[0], &glob_merge_displs[0], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        }
        else
        {
            MPI_Gatherv(u.ptr(over_first), ls_size*n_ext, MPI_DOUBLE, NULL, NULL, NULL, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        }

        return u_glob;
    }

    Mat &GetSolution()
    {
        Mat &u = MergeToMaster(u_curr, u_glob);
        err_total = getError(u, t0);
        return u;
    }

    void MakeVertPass(Mat &u0, Mat &u1, double t0)
    {
        double tu = t0 + tau;
        double tf = t0 + tau/2;
        
        int ls_ofs = range_start - ls_start;

        //compute upper bound
        if (idx_proc == 0)
        {
            #pragma omp parallel for schedule(static)
            for (int j = 0; j < n_ext; j++)
                pb_d.at(0, j) = u1.at(0, j) = TaskDef::u(0, j*h, tu);
        }

        //compute right part of equation
        #pragma omp parallel for
        for (int i = 1; i < range_size_ext - 1; i++)
        {
            int i_glob = i + range_start_ext;

            double *u_cur = &u0.at(i+0, 0);
            double *d_cur = pb_d.ptr(i - 1 + ls_ofs);

            for (int j = 1; j < n_ext - 1; j++)
            {
                d_cur[j] = -h2k*TaskDef::f(i_glob*h, j*h, tf) - u0.at(i, j-1) + (2.0 - h2ktau)*u0.at(i, j) - u0.at(i, j+1);
            }
        }

        //compute lower bound
        if (idx_proc == num_procs-1)
        {
            #pragma omp parallel for schedule(static)
            for (int j = 0; j < n_ext; j++)
                pb_d.at(ls_size-1, j) = u1.at(range_size_ext-1, j) = TaskDef::u(1, j*h, tu);
        }
        
        #pragma omp parallel
        {
            int start, end;
            GetRange(omp_get_thread_num(), omp_get_num_threads(), 1, n_ext - 1, start, end);

            //fwd
            for (int i = 1; i < ls_size; i++)
            {
                double *d_prv = pb_d.ptr(i - 1);
                double *d_cur = pb_d.ptr(i + 0);
                double coef = pb_dcoef_fwd[ls_start + i];

                for (int j = start; j < end; j++)
                    d_cur[j] += coef * d_prv[j];
            }

            //bwd
            for (int i = (ls_size - 2) - 1; i >= 0; i--)
            {
                double *d_nxt = pb_d.ptr(i + 1);
                double *d_cur = pb_d.ptr(i + 0);
                double coef = pb_dcoef_bwd[ls_start + i];

                for (int j = start; j < end; j++)
                    d_cur[j] += coef * d_nxt[j];
            }
        }

        //update last row
        if (idx_proc > 0)
            MPI_Isend(pb_d.ptr(0), n_ext, MPI_DOUBLE, idx_proc - 1, get_tag(TAG_D_NBR_UPDATE, iter), MPI_COMM_WORLD, &request);
        if (idx_proc < num_procs - 1)
        {
            MPI_Recv(pb_d.ptr(ls_size), n_ext, MPI_DOUBLE, idx_proc + 1, get_tag(TAG_D_NBR_UPDATE, iter), MPI_COMM_WORLD, &status);
            for (int j = 1; j < n_ext; j++)
                pb_d.at(ls_size - 1, j) += pb_dcoef_bwd[ls_start + ls_size - 1] * pb_d.at(ls_size, j);
        }

        if (pb_use_all2all)
            ComputeSeamRows_AllToAll(u1);
        else
            ComputeSeamRows_Gather(u1);

        //compute solutions for other rows using the last one
        #pragma omp parallel for
        for (int i = 0; i < ls_size - 1; i++)
        {
            double *x_fst = u1.ptr(0);
            double *x_lst = u1.ptr(ls_i0 + ls_size - 1);
            double *x_cur = u1.ptr(ls_i0 + i);
            double *d_cur = pb_d.ptr(i);
            double g = pb_g[ls_start + i];
            double a = pb_a[ls_start + i];
            double f = pb_f[ls_start + i];
            assert(!(idx_proc == 0) || f == 0);

            if (idx_proc > 0)
            {
                for (int j = 1; j < n_ext - 1; j++)
                    x_cur[j] = (d_cur[j] - g * x_lst[j] - f * x_fst[j]) / a;
            }
            else
            {
                for (int j = 1; j < n_ext - 1; j++)
                    x_cur[j] = (d_cur[j] - g * x_lst[j]) / a;
            }
        }

        //set answer on L/R bounds
        #pragma omp parallel for schedule(static)
        for (int i = 0; i < range_size_ext; i++)
        {
            int i_glob = i + range_start_ext;
            u1.at(i, 0) = TaskDef::u(i_glob*h, 0, tu);
            u1.at(i, n_ext - 1) = TaskDef::u(i_glob*h, 1, tu);
        }

        //send overlapped rows to nbrs
        SyncOverlappedRows(u1, TAG_SYNC_OVERLAPPED_ROWS_VER);

        //check very first and last rows
        if (idx_proc == 0)
            for (int j = 0; j < n_ext; j++)
                assert( abs(u1.at(0, j) - TaskDef::u(0, j*h, tu)) < EPS);
        if (idx_proc == num_procs - 1)
            for (int j = 0; j < n_ext; j++)
                assert( abs(u1.at(range_size_ext - 1, j) - TaskDef::u(1, j*h, tu)) < EPS);
    }

    void ComputeSeamRows_AllToAll(Mat &u1)
    {
        MPI_Alltoallv(pb_d.ptr(ls_size-1), &pm_dsnd_counts[0], &pm_dsnd_displ[0], MPI_DOUBLE, pm_d.ptr(), &pm_drcv_counts[0], &pm_drcv_displs[0], MPI_DOUBLE, MPI_COMM_WORLD);
        SolveMergeSystem(pm_d, pm_x, true);
        MPI_Alltoallv(pm_x.ptr(), &pm_xsnd_counts[0], &pm_xsnd_displs[0], MPI_DOUBLE, &x_fst_lst_buf[0], &pm_rcv_back_counts[0], &pm_rcv_back_displs[0], MPI_DOUBLE, MPI_COMM_WORLD);

        if (idx_proc == 0)
        {
            memcpy(u1.ptr(ls_i0 + ls_size - 1) + 1, &x_fst_lst_buf[0], (n_ext-2)*sizeof(double));
        }
        else
        {
            for (int i = 0; i < num_procs; i++)
            {
                int ofs = pm_rcv_back_displs[i];
                int cnt = pm_rcv_back_counts[i] / 2;
                int ofs_dst = 1 + ofs/2;
                assert(cnt * 2 == pm_rcv_back_counts[i]);

                memcpy(u1.ptr(0) + ofs_dst, &x_fst_lst_buf[ofs], cnt * sizeof(double));
                memcpy(u1.ptr(ls_i0 + ls_size - 1) + ofs_dst, &x_fst_lst_buf[ofs + cnt], cnt * sizeof(double));
            }
        }
    }

    void ComputeSeamRows_Gather(Mat &u1)
    {
        //send to merger
        MPI_Gather(pb_d.ptr(ls_size - 1), n_ext, MPI_DOUBLE, pm_d.ptr(), n_ext, MPI_DOUBLE, num_procs - 1, MPI_COMM_WORLD);
        if (idx_proc == num_procs - 1)
        {
            SolveMergeSystem(pm_d, pm_x, false);

            for (int i_proc = 0; i_proc < num_procs; i_proc++)
                MPI_Isend(pm_x.ptr(i_proc), n_ext, MPI_DOUBLE, i_proc, get_tag(TAG_X_FROM_MERGER1, i_proc), MPI_COMM_WORLD, &request);
            for (int i_proc = 1; i_proc < num_procs; i_proc++)
                MPI_Isend(pm_x.ptr(i_proc - 1), n_ext, MPI_DOUBLE, i_proc, get_tag(TAG_X_FROM_MERGER2, i_proc), MPI_COMM_WORLD, &request);
        }

        //receive solution for first and last row from merger
        MPI_Recv(u1.ptr(ls_i0 + ls_size - 1), n_ext, MPI_DOUBLE, num_procs - 1, get_tag(TAG_X_FROM_MERGER1, idx_proc), MPI_COMM_WORLD, &status);
        if (idx_proc > 0)
            MPI_Recv(u1.ptr(0), n_ext, MPI_DOUBLE, num_procs - 1, get_tag(TAG_X_FROM_MERGER2, idx_proc), MPI_COMM_WORLD, &status);
    }

    void SolveMergeSystem(Mat &d, Mat &x, bool with_borders = false)
    {
        const int ofs = with_borders ? 0 : 1;
        const int w = d.w;

        for (int i = 1; i < num_procs; i++)
        {
            double *d_prv = d.ptr(i - 1);
            double *d_cur = d.ptr(i + 0);
            double coef = pm_coef[i];

            for (int j = ofs; j < w - ofs; j++)
                d_cur[j] += coef * d_prv[j];
        }

        for (int j = ofs; j < w - ofs; j++)
            x.at(num_procs-1, j) = d.at(num_procs-1, j) / pm_c_[num_procs-1];

        for (int i = num_procs - 2; i >= 0; i--)
        {
            double *d_cur = d.ptr(i + 0);
            double *x_nxt = x.ptr(i + 1);
            double *x_cur = x.ptr(i + 0);
            double b = pm_b[i];
            double c_ = pm_c_[i];

            for (int j = ofs; j < w - ofs; j++)
                x_cur[j] = d_cur[j]/c_ - x_nxt[j] * (b/c_);
        }
    }

    void solve(int num_iters, double t_reach)
    {
        if (t_reach > 0)
        {
            num_iters = (int)ceil(t_reach / tau);
            tau = t_reach / num_iters;
        }
        t_reach = tau * num_iters;

        double t_half, t_next;

        t0 = 0;
        iter = 0;
        SetU(u_curr, 0, range_start_ext);

        for (; iter < num_iters; iter++)
        {
            t0 = iter * tau;
            t_half = t0 + tau/2;
            t_next = t0 + tau;

            MakeHorPass(u_curr, u_next, t0);
            MakeVertPass(u_next, u_curr, t0);

            if (print_each > 0 && iter % print_each == 0)
            {
                if (is_master()) pprintf("iter %03d (t=%.3lf)\n", iter, t0);

                //double app_half = getApproxError(u_cur, u_nxt, t_half, true);
                //double err_half = getError(u_nxt, t_half, true);
                //double app_full = getApproxError(u_nxt, u_cur, t_half, false);
                //double err_full = getError(u_cur, t_next, true);
                //pprintf("app_half=%.8lf err_half=%.8f\n", app_half, err_half);
                //pprintf("app_full=%.8lf err_full=%.8lf\n", app_full, err_full);

                MergeToMaster(u_curr, u_glob);
                if (idx_proc == 0)
                {
                    pprintf("Total err=%.8lf\n", getError(u_glob, t_next));
                    //u_glob.DumpCSV("u.txt");
                }
            }
        }

        t0 += tau;
        assert(abs(t0 - t_reach) < EPS);
    }
};

int main(int argc, char **argv)
{
    std::ios::sync_with_stdio(true);

    MPI_Init(&argc, &argv);

    int N, num_iters;
    double T;
    parse_args(argc, argv, N, num_iters, T);

    int num_procs, idx_proc;
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &idx_proc);
    if (num_procs > N)
    {
        MPI_Finalize();
        exit(-1);
    }

    SolverMPI solver(N, 4);
    solver.print_each = 0;

    double tstart = MPI_Wtime();
    solver.solve(num_iters, T);
    tstart = MPI_Wtime() - tstart;

    Mat &u = solver.GetSolution();
    if (idx_proc == 0)
    {
        string sfn = string_format("solution_N%d_t%lf_P%d.csv", N, solver.t0, num_procs);

        printf("{\n");
        printf("\"N\"     : %d,\n", N);
        printf("\"P\"     : %d,\n", num_procs);
        printf("\"P_omp\" : %d,\n", solver.num_omp_procs);
        printf("\"time\"  : %lf,\n", tstart);
        printf("\"err\"   : %lf,\n", getSolutionError(u, solver.t0));

        printf("\"t0\"    : %lf,\n", solver.t0);
        printf("\"iters\" : %d,\n", solver.iter);
        printf("\"file\"  : \"%s\"", sfn.c_str());
        printf("}\n");

        //u.DumpCSV(sfn);
    }

    MPI_Finalize();

    return 0;
}