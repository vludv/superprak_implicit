#pragma once
#include <cmath>
#include <cstring>
#include <iostream>
using std::sin;
using std::cos;
using std::exp;

#define PI 3.1416
#define SQR(x) ((x)*(x))

class TaskDef
{
public:
    inline static double u(double y, double x, double t)
    {
        return sin(2.0*PI*(x + t)) * cos(2*PI*y);
    }

    inline static double f(double y, double x, double t)
    {
        return 2*PI * cos(2*PI*(x+t))*cos(2*PI*y) + k()*8*SQR(PI) * u(y, x, t);
    }

    inline static double k()
    {
        return 1.0;
    }
};

#define  EPS 0.000001

class ISolver
{
public:

    int n, n_ext;
    double tau, h, k;

    ISolver(int N)
    {
        n = N;
        n_ext = n + 2;

        h = 1.0 / (n_ext - 1);
        k = TaskDef::k();

        tau = (2 * h*h) / sin(PI*h) / 10;
    }

    virtual void solve(int num_iters, double t_reach)
    {

    }

    virtual ~ISolver()
    {
    }
};

#include <stdarg.h>  // For va_start, etc.
#include <memory>    // For std::unique_ptr

static std::string string_format(const std::string fmt_str, ...) {
    int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
    std::string str;
    std::string formatted;
    va_list ap;
    while (1) {
        formatted.resize(n);
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return formatted;
}

int parse_args(const int argc, char *const*argv, int &N, int &num_iters, double &T)
{
    if (argc < 2)
    {
        std::cerr << "<N> <num_iters>" << std::endl;
        return -1;
    }

    N = atoi(argv[1]);

    T = num_iters = 0;
    if (std::strstr(argv[2], "."))
        T = atof(argv[2]);
    else
        num_iters = atoi(argv[2]);

    return 0;
}