import sys, os, shutil, subprocess, re, itertools, math

t_lims = {1024:180, 512:300, 256:600, 128:900, 64:7200, 32:7200, 1:7200}

n_list = [1024, 2048, 4096, 8192]
#n_list = [4096, 8192]
p_list = [1, 64, 128, 256, 512, 1024]
t = 0.01

ref_size = 4096
ref_iters = 100

def solver(p):
    if p == 1:
        return "solver_st"
    else:
        return "solver_mpi"

def est_time(n, procs):
    total = t_lims[procs]
    h = total / 3600
    m = (total - h*3600) / 60
    s = total - h*3600 - m*60
    return ("%02d:%02d:%02d" % (h, m, s))

if __name__ == "__main__":
    for n in n_list:
        for p in p_list:
            wtime = est_time(n, p)
            exe = os.path.abspath( solver(p) )
            stdout = "runs/n%d_p%d..out" % (n, p)
            stderr = "runs/stderr/n%d_p%d..out" % (n, p)
            env = 'OMP_NUM_THREADS=4'
            iters = max(1, ref_iters * ref_size**2 / n**2)
            
            if os.path.exists(stdout) and os.path.getsize(stdout) > 0:
                print stdout, "already exists"
                continue
            
            cmd = 'mpisubmit.bg -n %d -m SMP -w %s -e "%s" --stdout "%s" --stderr "%s"   "%s" -- %d %d' % (p, wtime, env, stdout, stderr, exe, n, iters)
            print cmd, "\n"
            
            if os.system(cmd):
                print "Failed"