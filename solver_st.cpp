#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <mpi.h>
#include "task_def.hpp"
using namespace std;

class Mat
{
public:

    int n, n_ext;
    double *data, *data_raw;

    Mat()
    {
        n = n_ext = 0;
        data = data_raw = NULL;
    }

    Mat(int _n)
    {
        n = _n;
        n_ext = n + 2;

        data_raw = new double[n_ext*n_ext];
        data = data + n_ext + 1;
    }

    inline double &at(int i, int j)
    {
        return data_raw[i*n_ext + j];
    }

    inline double &operator()(int i, int j)
    {
        return data_raw[i*n_ext + j];
    }

    void DumpCSV(const string &filename)
    {
        FILE *f = fopen(filename.c_str(), "w");

        for (int i = 0; i < n_ext; i++)
        {
            for (int j = 0; j < n_ext; j++)
            {
                fprintf(f, "%.3lf", at(i, j));
                if (j < n_ext-1)
                    fprintf(f, ",");
            }

            if (i < n_ext-1)
                fprintf(f, "\n");
        }

        fclose(f);
    }

    ~Mat()
    {
        delete[] data_raw;
        data = data_raw = NULL;
        n = n_ext = 0;
    }
};

static double getSolutionError(Mat &u, double t)
{
    double err = 0;
    double nrm = u.n_ext - 1;

    for (int i = 0; i < u.n_ext; i++)
        for (int j = 0; j < u.n_ext; j++)
        {
            double ref = TaskDef::u(i / nrm, j / nrm, t);
            double est = u.at(i, j);
            err = std::max(err, abs(ref - est));
        }

    return err;
}

class SolverST : public ISolver
{
    vector<double> c_, f_, f_coef, x_coef;
    double h2tau, h2ktau, h2k;
    Mat f_buf;
    vector<double> u0, u1;

public:
    Mat u, u_half;

    int print_each;
    int iter;
    double t0;

    SolverST(int N) :
        ISolver(N),
        u(N), u_half(N), f_buf(N)
    {
        c_.resize(n_ext);
        f_.resize(n_ext);
        f_coef.resize(n_ext);
        x_coef.resize(n_ext);
        u0.resize(n_ext);
        u1.resize(n_ext);

        print_each = 10;

        h2tau = 2*h*h / tau;
        h2ktau = 2*h*h / (tau * k);
        h2k = h*h / k;

        c_[0] = C(0);
        f_coef[0] = NAN;
        for (int i = 1; i < n_ext; i++)
        {
            c_[i] = C(i) - A(i) * B(i-1) / c_[i-1];
            f_coef[i] = -A(i) / c_[i-1];
            x_coef[i] = -B(i) / c_[i];
        }
    }

    ~SolverST()
    {
    }

    void SetU(Mat &u, double t = 0)
    {
        for (int i = 0; i < n_ext; i++)
            for (int j = 0; j < n_ext; j++)
                u.at(i, j) = TaskDef::u(i*h, j*h, t);
    }

    void SetBoundsVer(Mat &u, double t)
    {
        for (int i = 0; i < n_ext; i++)
        {
            u.at(i, 0) = TaskDef::u(i*h, 0.0, t);
            u.at(i, n_ext-1) = TaskDef::u(i*h, 1.0, t);
        }
    }

    void SetBoundsHor(Mat &u, double t)
    {
        for (int j = 0; j < n_ext; j++)
        {
            u.at(0, j) = TaskDef::u(0.0, j*h, t);
            u.at(n_ext - 1, j) = TaskDef::u(1.0, j*h, t);
        }
    }

    //definition of 3-diagonal matrix
    double A(int i)
    {
        return (0 < i && i < n_ext - 1) ? 1 : 0;
    }
    double C(int i)
    {
        return (0 < i && i < n_ext - 1) ? (-2 - h2ktau) : 1;
    }
    double B(int i)
    {
        return (0 < i && i < n_ext - 1) ? 1 : 0;
    }

    void MakeHorPass(Mat &u, Mat &u_next, double t0)
    {
        double tf = t0 + tau/2;
        double tu = t0 + tau/2;
        SetBoundsVer(u_next, tu);
        SetBoundsHor(u_next, tu);

        for (int i = 1; i < n_ext-1; i++)
        {
            double *x = &u_next.at(i, 0);
            double x0 = TaskDef::u(i*h, 0.0, tu);
            double x1 = TaskDef::u(i*h, 1.0, tu);

            f_[0] = x0;
            for (int j = 1; j < n_ext-1; j++)
            {
                double f = -h2k*TaskDef::f(i*h, j*h, tf) - u.at(i-1, j) + (2.0 - h2ktau)*u.at(i, j) - u.at(i+1, j);
                f_[j] = f + f_coef[j] * f_[j-1];
            }
            //f_[n_ext-1] = x1; //useless
            
            x[n_ext-1] = x1;
            for (int j = n_ext - 2; j >= 0; j--)
                x[j] = f_[j]/c_[j] + x_coef[j] * x[j+1];
            assert(abs(x0 - x[0]) < EPS);
        }
    }

    void MakeVertPass(Mat &u, Mat &u_next, double t0)
    {
        double tf = t0 + tau/2;
        double tu = t0 + tau;
        SetBoundsHor(u_next, tu);
        SetBoundsVer(u_next, tu);

        for (int j = 1; j < n_ext - 1; j++)
            f_buf.at(0, j) = TaskDef::u(0.0, j*h, tu);

        for (int i = 1; i < n_ext - 1; i++)
        {
            double *f_cur = &f_buf.at(i, 0);
            double *f_prv = &f_buf.at(i - 1, 0);

            for (int j = 1; j < n_ext - 1; j++)
            {
                double f = -h2k*TaskDef::f(i*h, j*h, tf) - u.at(i, j-1) + (2.0 - h2ktau)*u.at(i, j) - u.at(i, j+1);
                f_cur[j] = f + f_coef[i] * f_prv[j];
            }
        }

        for (int j = 1; j < n_ext - 1; j++)
            u_next.at(n_ext-1, j) = TaskDef::u(1.0, j*h, tu);

        for (int i = n_ext - 2; i >= 0; i--)
        {
            double *f_cur = &f_buf.at(i, 0);
            double *u_cur = &u_next.at(i, 0);
            double *u_nxt = &u_next.at(i + 1, 0);

            for (int j = 1; j < n_ext - 1; j++)
                u_cur[j] = f_cur[j] / c_[i] + x_coef[i] * u_nxt[j];
        }
    }

    void solve(int num_iters = 1000, double t_reach = -1)
    {
        if (t_reach > 0)
        {
            num_iters = (int)ceil(t_reach / tau);
            tau = t_reach / num_iters;
        }
        t_reach = tau * num_iters;

        SetU(u, 0);
        if (print_each > 0)
            u.DumpCSV("u_start.csv");

        iter = 0;
        t0 = 0;
        double approx_half0_error, approx_half1_error;

        for (; iter < num_iters; iter++)
        {
            bool print_dbg = (print_each > 0 && iter % print_each == 0);

            t0 = tau*iter;

            MakeHorPass(u, u_half, t0);
            if (print_dbg) approx_half0_error = getApproxError(u, u_half, t0 + tau/2, true);
            MakeVertPass(u_half, u, t0);
            if (print_dbg) approx_half1_error = getApproxError(u_half, u, t0 + tau/2, false);

            if (print_dbg)
            {
                printf("iter #%04d t=%.6lf err=%.6lf\n", iter, t0, getSolutionError(u, t0+tau));
                printf("app_err: half0=%.9lf half1=%.9lf can=%.9lf\n", approx_half0_error, approx_half1_error, getApproxErrorCanonical(u_half, u, t0 + tau, tau/2) );
                
                u.DumpCSV(string_format("u_iter_%04d.csv", iter));
                SetU(u_half, t0+tau);
                u_half.DumpCSV(string_format("u_iter_%04d_ideal.csv", iter));
            }
        }

        t0 = tau*num_iters;
    }

    double getApproxError(Mat &u0, Mat &u1, double t, bool half)
    {
        double err = 0;
        double nrm = u0.n_ext - 1;

        for (int i = 1; i < u0.n_ext - 1; i++)
            for (int j = 1; j < u0.n_ext - 1; j++)
            {
                double l = (u1(i, j) - u0(i, j))/(tau/2);
                double r = TaskDef::f(i*h,j*h,t);
                if (half)
                    r += TaskDef::k()*(u1(i,j-1) -2*u1(i,j) + u1(i,j+1) + u0(i-1,j) -2*u0(i,j) + u0(i+1,j))/(h*h);
                else
                    r += TaskDef::k()*(u0(i,j-1) -2*u0(i,j) + u0(i,j+1) + u1(i-1,j) -2*u1(i,j) + u1(i+1,j))/(h*h);
                double cur_err = abs(l-r);

                err = std::max(err, cur_err);
            }

        return err;
    }

    double getApproxErrorCanonical(Mat &u0, Mat &u1, double t1, double tau)
    {
        double err = 0;
        double nrm = u0.n_ext - 1;

        for (int i = 1; i < u0.n_ext - 1; i++)
            for (int j = 1; j < u0.n_ext - 1; j++)
            {
                double ut = (u1(i,j) - u0(i, j)) / tau;
                double ux = TaskDef::k()*(u0(i,j-1) -2*u0(i,j) + u0(i,j+1) + u1(i-1,j) -2*u1(i,j) + u1(i+1,j))/(h*h) + TaskDef::f(i*h, j*h, t1-tau/2);
                //double ux1 = TaskDef::k()*(u0(i,j-1) -2*u0(i,j) + u0(i,j+1) + u0(i-1,j) -2*u0(i,j) + u0(i+1,j))/(h*h) + TaskDef::f(i*h, j*h, t1-tau);
                //double ux2 = TaskDef::k()*(u1(i,j-1) -2*u1(i,j) + u1(i,j+1) + u1(i-1,j) -2*u1(i,j) + u1(i+1,j))/(h*h) + TaskDef::f(i*h, j*h, t1);
                //double ux = (ux1 + ux2) * 0.5;
                double dif = abs(ut - ux);
                err = std::max(err, dif);
            }

        return err;
    }

};

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    
    int num_procs, idx_proc;
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &idx_proc);
    if (idx_proc != 0)
    {
        MPI_Finalize();
        exit(0);
    }
    num_procs = 1;

    int N, num_iters;
    double T;
    parse_args(argc, argv, N, num_iters, T);

    SolverST solver(N);
    solver.print_each = -1;

    double tstart = MPI_Wtime();
    solver.solve(num_iters, T);
    tstart = MPI_Wtime() - tstart;

    Mat &u = solver.u;
    string sfn = string_format("solution_N%d_t%lf_P%d.csv", N, solver.t0, num_procs);

    printf("{\n");
    printf("\"N\"     : %d,\n", N);
    printf("\"P\"     : %d,\n", num_procs);
    printf("\"time\"  : %lf,\n", tstart);
    printf("\"err\"   : %lf,\n", getSolutionError(u, solver.t0));

    printf("\"t0\"    : %lf,\n", solver.t0);
    printf("\"iters\" : %d,\n", solver.iter);
    printf("\"file\"  : \"%s\"", sfn.c_str());
    printf("}\n");

    //u.DumpCSV(sfn);

    MPI_Finalize();

    return 0;
}